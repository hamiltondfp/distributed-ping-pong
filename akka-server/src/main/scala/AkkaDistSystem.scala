import akka.actor.{ ActorRef, ActorSystem, Props, Actor, Inbox }
import scala.concurrent.duration._
import akka.actor.Terminated
import akka.contrib.pattern.ClusterReceptionistExtension
import akka.contrib.pattern.DistributedPubSubExtension
import akka.contrib.pattern.DistributedPubSubMediator.Subscribe
import akka.contrib.pattern.DistributedPubSubMediator.SubscribeAck
import akka.contrib.pattern.ClusterSharding
import akka.contrib.pattern.ShardRegion.IdExtractor
import akka.contrib.pattern.ShardRegion.ShardResolver
import akka.cluster.Cluster
import akka.cluster.MemberStatus

object AkkaDistServer extends App {
  
  val system = ActorSystem("distTest")
  
  if(Cluster(system).selfRoles.exists { _.startsWith("matcher")}) {
    val a = system.actorOf(Props(new Matcher()), s"matcher")
    println("************************************** " + a.path)
  }
  
  case class Client(ref: ActorRef, score: Int) {
    def gainPoint: Client = Client(ref, score + 1)
  }
}




