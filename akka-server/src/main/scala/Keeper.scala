import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import AkkaDistServer.Client


class Keeper extends Actor {

  val random = new util.Random()
  
  def receive = {
    case (first: ActorRef, second: ActorRef, id: Long) => {
      println("here?")
      context.become(play(Client(first, 0), Client(second, 0), id))
      first ! ("pong", id)
    }
  }
  
  def play(c1: Client, c2: Client, id: Long): Receive = {
    case ref: ActorRef => {
      println("playing a round")
      println(ref.toString)
      println(c1.ref.toString)
      println(c2.ref.toString)
      val (from, to) = 
          if(ref.toString == c1.ref.toString) {
            println("from c1")
            (c1, c2)
          } 
          else {
            println("from c2")
            (c2, c1)
          }
      if(miss) {
        println("miss!")
        if(from.score == 9){
          from.ref ! "win"
          to.ref ! "lose"
          context stop self
        }
        else {
          val newFrom = from.gainPoint
          context.become(play(newFrom, to, id))
          printScore(newFrom, to)
          to.ref ! ("pong", id)
        }
      } else {
        println(s"sending pong to ${to.ref}")
        to.ref ! ("pong", id)
      }
    }
  }
  
  private def miss: Boolean = random.nextInt(10) == 0
  
  private def printScore(c1: Client, c2: Client) {
    val (high, low) = if(c1.score > c2.score) (c1, c2) else (c2, c1)
    println(s"score is now: ${high.score} - ${low.score}")
  }
}

